const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config');

exports.Authenticated = function(req, res, next) {

    if(!req.cookies.authcookie) {

        return res
        .redirect('/iniciarSesion');
    }
  
    let token = req.cookies.authcookie;

    try{

        let payload = jwt.decode(token, config.TOKEN_SECRET);
        req.usuario = payload.sub;

        next();
    }
    catch(error){

        res.clearCookie("authcookie");
        res.status(200);

        return console.log(error);
    }   
}