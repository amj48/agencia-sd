'use strict'

const port = process.env.PORT || 3010
const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');

//conexion segura
const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {

    key: fs.readFileSync('./cert/key.pem'),
    certs: fs.readFileSync('./cert/cert.pem'),
    ciphers: "DEFAULT:!SSLv2:IRC4:!EXPORT:!LOW:!MEDIUM:!SHA1"
};

//BBDD
var mongoose = require('mongoose');
var urlDB = 'mongodb+srv://sd:sd@cluster0.1jc0t.mongodb.net/Agencia?retryWrites=true&w=majority';
mongoose.connect(urlDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));
mongoose.connect(urlDB, {useNewUrlParser: true } )

//librerias
const app = express();
app.use(express.static('/assets-home'));
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}))
app.use(cookieParser());

//Rutas
app.get('/index', (req,res,next) => {

    res.status(200);
    res.render('../views/index.ejs');
})

app.get('/registro', (req,res,next) => {

    console.log("INDEX");
    res.render('../views/registro.ejs');
});

var vehiculos = require('./routes/Vehiculos');
app.use('/', vehiculos);

var vuelos = require('./routes/Vuelos');
app.use('/', vuelos);

var hoteles = require('./routes/Hoteles');
app.use('/', hoteles);

var paquetes = require('./routes/Paquetes');
app.use('/', paquetes);

var ususarios = require('./routes/Usuarios');
app.use('/', ususarios);

var transacciones = require('./routes/Transacciones');
app.use('/', transacciones);

app.get('/iniciarSesion', (req, res, next) => {
    
    if(!req.u){

        res.status(200);
        res.render('../views/iniciarSesion.ejs');
    }
    else{

        res.redirect('/index');
    }
});

// Seguridad
/*
https.createServer(OPTIONS_HTTPS,app).listen(port,() => {
    console.log(`API REST ejecutándose en https://localhost:${port}/index`);
})*/

// Iniciamos la aplicación
app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/index`);
});