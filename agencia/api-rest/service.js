const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('./config');

exports.createToken = function(user) {

  var payload = {

    sub: user._id,
    iat: moment().unix(),
    exp: moment().add('m', 60).unix(),
  };
  
  return jwt.encode(payload, config.TOKEN_SECRET);
};