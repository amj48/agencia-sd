const { ObjectID } = require('mongodb');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let ObjectId = require('mongodb').ObjectID;

var BancosSchema = new Schema({

    _id: {type: ObjectId, required: false, max:100},
    nombre: {type: String,requires:true,max:100},
    url: {type: String,requires:true,max:100},
});

module.exports = mongoose.model('Banco',BancosSchema);