var express = require('express');
var router = express.Router();
const auth = require('../middlewares/auth');

var vuelos = require('../controllers/VuelosController.js');

router.get('/listarVuelos',auth.Authenticated, vuelos.list);

module.exports = router;