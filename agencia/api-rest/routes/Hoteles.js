var express = require('express');
var router = express.Router();
const auth = require('../middlewares/auth');

var hoteles = require('../controllers/HotelesController.js');

router.get('/listarHoteles', auth.Authenticated, hoteles.list);

module.exports = router;