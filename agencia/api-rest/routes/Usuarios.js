var express = require('express');
var router = express.Router();

var usuarios = require('../controllers/UsuariosController.js');

router.post('/registro', usuarios.registrarse);
router.post('/iniciarSesion', usuarios.login);

module.exports = router;