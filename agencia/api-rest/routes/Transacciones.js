var express = require('express');
var router = express.Router();

var transaccion = require('../controllers/TransaccionesController');

router.post('/reservar', transaccion.transaccion);

module.exports = router;