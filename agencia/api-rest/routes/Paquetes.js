var express = require('express');
var router = express.Router();
const auth = require('../middlewares/auth');

var paquetes = require('../controllers/PaquetesController.js');

router.get('/listarPaquetes', auth.Authenticated, paquetes.list);

module.exports = router;