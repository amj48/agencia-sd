var express = require('express');
var router = express.Router();
const auth = require('../middlewares/auth');

var vehiculos = require('../controllers/VehiculosController.js');

router.get('/listarVehiculos', auth.Authenticated, vehiculos.list);

module.exports = router;