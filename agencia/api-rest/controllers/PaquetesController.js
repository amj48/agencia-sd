const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;

var PaquetesController = {};

PaquetesController.list = function(req, res){
        
    let tamano = Object.keys(req.query).length;
    let cadena = req.query.pais;

    if(tamano === 1){

        const urls = ["http://192.168.4.2:3001/listarPaquetes?pais=" + req.query.pais, "http://192.168.4.130:3001/listarPaquetes?pais=" + req.query.pais];

        Promise.all([

            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('paquete.ejs', {p: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
    else{

        const urls = ["http://192.168.4.2:3001/listarPaquetes", "http://192.168.4.130:3003/listarPaquetes"];

        Promise.all([

            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('paquete.ejs', {p: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
};

module.exports = PaquetesController;