var mongoose = require('mongoose');
var Usuarios = require("../models/Usuarios");
const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;
const service = require('../service');
const bcrypt = require('bcrypt');


var UsuariosController = {};

UsuariosController.registrarse = async function(req, res){

    console.log("Registrando usuario");

    const hashedPassword = await bcrypt.hash(req.body.contrasena,10);

    var u = new Usuarios({

        _id: new mongoose.mongo.ObjectId(),
        nombre: req.body.nombre, 
        correo: req.body.correo, 
        contrasena: hashedPassword
    });

    u.save(function (err){

        if(!err){

            let token = service.createToken(u);

            res.cookie('authcookie',token);
            res.redirect('/index');
        }
        else {

            console.log(err);
            res.redirect('/iniciarSesion');
        }
    });

    
};

UsuariosController.login = function(req, res){

    console.log("Logando usuario");
    
    Usuarios.findOne({correo: req.body.correo}, function(err, u) {
              
        if(!err){

            var result = bcrypt.compareSync(req.body.contrasena,u.contrasena);

            if(result){

                console.log(`La sesión por el usuario  ${u.correo} ha sido iniciada. `);

                let token = service.createToken(u);
                res.cookie('authcookie',token);
                res.redirect('/index');
            }
            else{

                console.log('La contraseña es incorrecta');
                res.redirect('/iniciarSesion');
            }
        }
        else{
            
            res.redirect('/iniciarSesion');
        }
    });
};

module.exports = UsuariosController;

