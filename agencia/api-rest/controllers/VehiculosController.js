const fetch = require('node-fetch');
const Bluebird = require('bluebird');
const moment = require('moment');
fetch.Promise = Bluebird;

var VehiculosController = {};

VehiculosController.list = function(req, res){
   
    let tamano = Object.keys(req.query).length;
    let cadena = req.query.tipo;

    if(tamano === 1){

        const urls = ["http://192.168.4.2:3001/listarVehiculos?tipo=" + req.query.tipo, "http://192.168.4.130:3003/listarVehiculos?tipo=" + req.query.tipo];

        Promise.all([

            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('vehiculo.ejs', {v: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
    else{

        const urls = ["http://192.168.4.2:3001/listarVehiculos", "http://192.168.4.130:3003/listarVehiculos"];

        Promise.all([

            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('vehiculo.ejs', {v: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
};

module.exports = VehiculosController;