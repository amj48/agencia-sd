var mongoose = require('mongoose');
const fetch = require('node-fetch');
const Bluebird = require('bluebird');
const moment = require('moment');
fetch.Promise = Bluebird;
var Transaccion = require("../models/Transacciones");
var Proveedor = require("../models/Proveedores");
var Banco = require("../models/Bancos");

const { response } = require('express');

var db = mongoose.connection;

const cabeceras= {

  'Content-Type': 'application/json',
  'Accept': 'application/json',
}

var transaccionController = {};

transaccionController.transaccion = async function(req, res){

    const session = await db.startSession();
    session.startTransaction();

    let newId = new mongoose.mongo.ObjectId();

    await Transaccion.create([{ 

        _id: newId, idUsuario: req.body.idUsuario , idProveedor: req.body.idProveedor, idProducto: req.body.idProducto}], { session: session }
    );

    let proveedor = await Proveedor.findOne({ _id: req.body.idProveedor});
    let banco = await Banco.findOne({ _id: req.body.idBanco});

    let urlProveedor = new URL(proveedor.url);
    let urlBanco = new URL(banco.url);

    const payload1 = {

        coleccion: req.body.coleccion, idP: req.body.idProducto
    };

    const request = {

      method: 'POST', 
      headers: cabeceras,
      body: JSON.stringify(payload1),
    };

    Promise.all([

      fetch(`${urlProveedor}preparacion`, request).then(resp => resp.json()).catch(error => console.log(error)),
      fetch(`${urlBanco}pagar`, request).then(resp => resp.json()).catch(error => console.log(error))

    ]).then(function(resultado){

        console.log(resultado);

        if(resultado[0] && resultado[1] && resultado[0].result === 'correcto' && resultado[1].result === 'correcto'){

            const pago = {

                coleccion: req.body.coleccion, idP: req.body.idProducto
            };

            const request = {

                method: 'POST', 
                headers: cabeceras,
                body: JSON.stringify(pago),
            };

            fetch(url + 'confirmacion', request)

            .then( response => response.json() )
            .then(async function(resultado){

                console.log(resultado);

                if(resultado.result === 'correcto'){

                    await session.commitTransaction();
                    session.endSession();
                
                    res.status(200);
                    res.json({
                        result: 'Transaccion completada'
                    });
                    res.end();

                    return;
                }
            });
        }
        else if(resultado[0].result === 'correcto'){

            const pagoRollback = {

                idProv: resultado[0].idProv
            };

            const requestRollback = {

                method: 'POST', 
                headers: cabeceras,
                body: JSON.stringify(pagoRollback),
            };
            
            fetch(url + 'revertir', requestRollback)

            .then( response => response.json() )
            .then(async function(resultado){

                res.status(200);
                res.json({
                    result:'Transaccion abortada'
                });
                res.end();

                await session.abortTransaction();
                session.endSession();

                return;

            });
        }
        else{

            throw new Error('Sistema ocupado!');
        }
    })
    .catch(async function(error){

        console.log(error);

        res.status(200);
        res.json({result:'Transaccion abortada'});
        res.end();

        await session.abortTransaction();
        session.endSession();
    }); 
}

module.exports = transaccionController;