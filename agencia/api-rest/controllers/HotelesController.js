const fetch = require('node-fetch');
const Bluebird = require('bluebird');
fetch.Promise = Bluebird;

var HotelesController = {};

HotelesController.list = function(req, res){
        
    let tamano = Object.keys(req.query).length;s

    if(tamano === 1){

        const urls = ["http://192.168.4.2:3001/listarHoteles?pais=" + req.query.pais, "http://192.168.4.130:3003/listarHoteles?pais=" + req.query.pais];

        Promise.all([
            
            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('hotel.ejs', {h: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
    else{

        const urls = ["http://192.168.4.2:3001/listarHoteles", "http://192.168.4.130:3003/listarHoteles"];

        Promise.all([
            
            fetch(urls[0]).then(resp => resp.json()).catch(error => console.log(error)),
            fetch([urls[1]]).then(resp => resp.json()).catch(error => console.log(error))

        ]).then(function(resultado){

            var productos = [];

            for(var i = 0; i < resultado.length; i++){

                if(resultado[i] !== undefined){

                    productos = productos.concat(resultado[i]);
                }
            }

            res.render('hotel.ejs', {h: productos});
        })
        .catch(function(error){

            console.log(error);
        })
    }
};

module.exports = HotelesController;