
var mongoose = require('mongoose');
var Paquetes = require("../models/Paquetes");

var PaquetesController = {};

PaquetesController.list = function(req, res){

    let tamano = Object.keys(req.query).length;

    if(tamano === 0 || tamano > 1 ){

        Paquetes.find({}).exec(function(err, paquetes){

            if( err ){ 
                
                console.log('Error: ', err); 
                return; 
            }

            console.log(paquetes);
            res.json(paquetes);
        });
    }
    else{

        Paquetes.find({"pais": req.query.pais}).exec(function(err, paquetes){

            if( err ){ 

                console.log('Error: ', err); 
                return; 
            }

            res.json(paquetes);
        });
    } 
};

module.exports = PaquetesController;