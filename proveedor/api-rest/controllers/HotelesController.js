
var mongoose = require('mongoose');
var Hoteles = require("../models/Hoteles");

var HotelesController = {};

HotelesController.list = function(req, res){

    let tamano = Object.keys(req.query).length;

    if(tamano === 0 || tamano > 1 ){

        Hoteles.find({}).exec(function(err, hoteles){

            if( err ){ 

                console.log('Error: ', err); 
                return; 
            }

            console.log(hoteles);
            res.json(hoteles);
        });
    }
    else{

        Hoteles.find({"pais": req.query.pais}).exec(function(err, hoteles){

            if( err ){ 
                
                console.log('Error: ', err); 
                return; 
            }

            res.json(hoteles);
        });
    } 
};

module.exports = HotelesController;