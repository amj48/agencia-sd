
var mongoose = require('mongoose');
var Vehiculos = require("../models/Vehiculos");

var VehiculosController = {};

VehiculosController.list = function(req, res){

    let tamano = Object.keys(req.query).length;

    if(tamano === 0 || tamano > 1 ){

        Vehiculos.find({}).exec(function(err, vehiculos){

            if( err ){ 

                console.log('Error: ', err); 
                return; 
            }

            console.log(vehiculos);
            res.json(vehiculos);
        });
    }
    else{

        Vehiculos.find({"tipo": req.query.tipo}).exec(function(err, vehiculos){

            if( err ){ 
                
                console.log('Error: ', err); 
                return; 
            }

            res.json(vehiculos);
        });
    } 
};

module.exports = VehiculosController;