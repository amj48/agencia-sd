
var mongoose = require('mongoose');
var Vuelos = require("../models/Vuelos");

var VuelosController = {};

VuelosController.list = function(req, res){

    let tamano = Object.keys(req.query).length;

    if(tamano === 0 || tamano > 1 ){

        Vuelos.find({}).exec(function(err, vuelos){

            if( err ){ 
                console.log('Error: ', err); 
                return; 
            }

            console.log(vuelos);
            res.json(vuelos);
        });
    }
    else{

        Vuelos.find({"pais": req.query.pais}).exec(function(err, vuelos){

            if( err ){ 
                
                console.log('Error: ', err); 
                return; 
            }

            res.json(vuelos);
        });
    } 

};

module.exports = VuelosController;