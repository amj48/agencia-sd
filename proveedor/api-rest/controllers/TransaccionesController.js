
var mongoose = require('mongoose');
var Transaccion = require("../models/Transacciones");

var TransaccionesController = {};

TransaccionesController.preparar = async function(req, res){

    let doc = await Transaccion.findOne({ nombreC: req.body.coleccion });

    if(doc !== null){

        res.status(200);
        res.json({result: 'rechazo'});
        res.end();
    }
    else if(doc === null){

        let newId = new mongoose.mongo.ObjectId();

        TransaccionNuevo = new Transaccion({ 

            _id: newId, nombreC: req.body.coleccion, idP: req.body.idP, status: 'pendiente'
        });

        await TransaccionNuevo.save();

        res.status(200);
        res.json({result: 'correcto', idProv: newId});
        res.end();
    }
};

TransaccionesController.revertir = async function(req, res){

    let doc = await Transaccion.deleteOne({_id: req.body.idProv});

    res.status(200);
    res.json({result: 'correcto'});
    res.end();
};

TransaccionesController.confirmar = async function(req, res){

    let doc = await Transaccion.findOne({ nombreC: req.body.coleccion });

    if(doc !== null){

        let coleccion;

        if(req.body.coleccion == 'vehiculos'){

            coleccion = require("../models/Vehiculos");
        }
        else if (req.body.coleccion == 'paquetes'){

            coleccion = require("../models/Paquetes");
        }
        else if (req.body.coleccion == 'vuelos'){
            
            coleccion = require("../models/Vuelos");
        }
        else if (req.body.coleccion == 'hoteles'){

            coleccion = require("../models/Hoteles");
        }

        let eliminarProducto = await coleccion.findOne({ _id: req.body.idP });
        eliminarProducto.cantidad = eliminarProducto.cantidad - 1;

        await eliminarProducto.save();
        await Transaccion.deleteOne({ nombreC: req.body.coleccion });

        res.status(200);
        res.json({result: 'correcto'});
        res.end();
    }
};

module.exports = TransaccionesController;