var express = require('express');
var router = express.Router();

var h = require('../controllers/HotelesController.js');

router.get('/listarHoteles', h.list);

module.exports = router;