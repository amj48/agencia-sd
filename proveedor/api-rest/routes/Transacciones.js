var express = require('express');
var router = express.Router();

var transaccion = require('../controllers/TransaccionesController');

router.post('/preparacion', transaccion.preparar);
router.post('/confirmacion', transaccion.confirmar);
router.post('/revertir', transaccion.revertir);

module.exports = router;