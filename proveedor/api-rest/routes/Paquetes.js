var express = require('express');
var router = express.Router();

var p = require('../controllers/PaquetesController.js');

router.get('/listarPaquetes', p.list);

module.exports = router;