var express = require('express');
var router = express.Router();

var vu = require('../controllers/VuelosController.js');

router.get('/listarVuelos', vu.list);

module.exports = router;