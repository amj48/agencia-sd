var express = require('express');
var router = express.Router();

var v = require('../controllers/VehiculosController.js');

router.get('/listarVehiculos', v.list);

module.exports = router;