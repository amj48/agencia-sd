'use strict'
const port = process.env.PORT || 3001
const express = require('express');
const logger = require('morgan');

//BBDD
var mongoose = require('mongoose');
var urlDB = 'mongodb+srv://sd:sd@cluster0.1jc0t.mongodb.net/Proveedor1?retryWrites=true&w=majority';
mongoose.connect(urlDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));
mongoose.connect(urlDB, {useNewUrlParser: true } )


const app = express();
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

// middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//rutas
var vehiculos = require('./routes/Vehiculos');
app.use('/', vehiculos);

var vuelos = require('./routes/Vuelos');
app.use('/', vuelos);

var hoteles = require('./routes/Hoteles');
app.use('/', hoteles);

var paquetes = require('./routes/Paquetes');
app.use('/', paquetes);

var transacciones = require('./routes/Transacciones');
app.use('/', transacciones);


// Iniciamos la aplicación
app.listen(port, () => {
    console.log(`API REST (parte Proveedor1) ejecutándose en http://localhost:${port}/api`);
});