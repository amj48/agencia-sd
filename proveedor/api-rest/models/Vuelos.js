
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VuelosSchema = new Schema({

    compania: {type: String,requires:true,max:100},
    pais: {type: String,requires:true,max:100},
    precio: {type: String,requires:true,max:100},
    imagen: {type: String,requires:true,max:100},
    cantidad: {type: Number,requires:true,max:100},
});

module.exports = mongoose.model('Vuelos',VuelosSchema);