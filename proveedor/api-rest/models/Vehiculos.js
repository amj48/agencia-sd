
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VehiculosSchema = new Schema({

    marca: {type: String,requires:true,max:100},
    modelo: {type: String,requires:true,max:100},
    precio: {type: String,requires:true,max:100},
    tipo: {type: String,requires:true,max:100},
    imagen: {type: String,requires:true,max:100},
    cantidad: {type: Number,requires:true,max:100},
});

module.exports = mongoose.model('Vehiculos',VehiculosSchema);