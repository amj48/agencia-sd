
const { ObjectID } = require('mongodb');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
let ObjectId = require('mongodb').ObjectID;

var TransaccionesSchema = new Schema({

    _id: {type: String,requires:true,max:100},
    nombreC: {type: String,requires:true,max:100},
    idP: {type: String,requires:true,max:100},
    status: {type: String,requires:true,max:100}  
});

module.exports = mongoose.model('Transacciones',TransaccionesSchema);