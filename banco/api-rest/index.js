'use strict'
const port = process.env.PORT || 3002
const express = require('express');
const logger = require('morgan');
const path = require('path');
const app = express();

// middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false})) 
app.use(express.json()) 

//rutas
var bancos = require('./routes/Bancos');
app.use('/', bancos);

// Iniciamos la aplicación
app.listen(port, () => {

    console.log(`API REST (parte Banco1) ejecutándose en http://localhost:${port}/api`);
});