var express = require('express');
var router = express.Router();

var banco = require('../controllers/BancoController');

router.post('/pagar', banco.check);

module.exports = router;